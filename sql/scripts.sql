-- Database: postgres

-- DROP DATABASE postgres;

CREATE DATABASE postgres
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Russian_Russia.1251'
       LC_CTYPE = 'Russian_Russia.1251'
       CONNECTION LIMIT = -1;

COMMENT ON DATABASE postgres
  IS 'default administrative connection database';

-- Some test values

CREATE TABLE category (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE category OWNER TO postgres;

INSERT INTO category VALUES (1, 'Games');
INSERT INTO category VALUES (2, 'Multimedia');
INSERT INTO category VALUES (3, 'Productivity');
INSERT INTO category VALUES (4, 'Tools');
INSERT INTO category VALUES (5, 'Health');
INSERT INTO category VALUES (6, 'Lifestyle');
INSERT INTO category VALUES (8, 'News');
INSERT INTO category VALUES (7, 'Sport');

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);

CREATE TABLE app (
  id integer NOT NULL,
  description character varying(255),
  downloads integer DEFAULT 0 NOT NULL,
  name character varying(255),
  pack character varying(255),
  big_picture_id integer,
  category_id integer,
  developer_id integer,
  small_picture_id integer
);


ALTER TABLE app OWNER TO postgres;

INSERT INTO app VALUES (1, 'App1', 100000, 'Super App', 'com.dataart', NULL, 1, NULL, NULL);
INSERT INTO app VALUES (3, 'App3', 1234532, 'Pop app', 'com.dataart', NULL, 1, NULL, NULL);
INSERT INTO app VALUES (4, 'App 4', 1234321, 'Mega app', 'com.dataart', NULL, 1, NULL, NULL);
INSERT INTO app VALUES (5, 'App5', 43242342, 'App App', 'com.dataart', NULL, 1, NULL, NULL);
INSERT INTO app VALUES (2, 'App2', 1000, 'Super puper app', 'com.dataart', NULL, 1, NULL, NULL);

ALTER TABLE ONLY app
  ADD CONSTRAINT app_pkey PRIMARY KEY (id);


