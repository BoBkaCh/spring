$.hash = '#!/';
$.container = '#pjaxcontainer';
$(document).pjax('a[data-pjax]', '#pjaxcontainer');

$(document).ready(function () {
    $('.popular').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1
    });

    var uploadDialog = $('#upload_form').dialog({
        autoOpen: false,
        height: 350,
        width: 500,
        modal: true,
        buttons: {
            Save: uploadApp,
            Cancel: function () {
                uploadDialog.dialog("close");
            }
        },
        close: function () {
        }
    });

    $('#upload_btn').click(function () {
        uploadDialog.dialog("open");
    });

    var logInDialog = $('#login_form').dialog({
        autoOpen: false,
        height: 610,
        width: 400,
        modal: true,
        buttons: {
            Cancel: function () {
                logInDialog.dialog("close");
            }
        },
        close: function () {
        }
    });

    $('#login_btn').click(function () {
        logInDialog.dialog("open");
    })
});

function uploadApp() {
    var formData = new FormData($('#upload_form')[0]);
    formData.append("appName", $('#new_app_name').val());
    formData.append("appDesc", $('#new_app_desc').val());
    formData.append("category", $('#new_app_category').val());
    $.ajax({
        url: "/app/upload",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false
    }).done(function (id) {
        // $.pjax({url: '/app/' + id, container: "#pjaxcontainer"});
        window.location.href = '/app/' + id;
    })
}

function registrate() {
    var error = $('#error');
    var user = {};
    error.hide();
    user.firstName = $('#first_name').val();
    user.lastName = $('#last_name').val();
    user.email = $('#email').val();
    user.password = $('#password').val();
    user.developer = $('#is_dev').is(':checked');

    $.ajax({
        type: 'POST',
        url: '/registrate',
        contentType: 'application/json',
        data: JSON.stringify(user)
    }).done(function () {
        window.location.reload();
    }).fail(function (response) {
        error.text(response.responseJSON.message);
        error.show();
    })
}

function logout() {
    $.post("/logout").done(function () {
        window.location.reload();
    })
}