package com.dataart.service;

public interface ImageService {

    byte[] find(Integer id);
}
