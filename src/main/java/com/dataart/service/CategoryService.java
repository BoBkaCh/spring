package com.dataart.service;

import com.dataart.model.Category;

public interface CategoryService {

    Category findOne(Integer id);

    Iterable<Category> findAll();
}
