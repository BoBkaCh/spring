package com.dataart.service.Impl;

import com.dataart.dao.ImageRepository;
import com.dataart.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public byte[] find(Integer id) {
        return imageRepository.findOne(id).getImage();
    }
}
