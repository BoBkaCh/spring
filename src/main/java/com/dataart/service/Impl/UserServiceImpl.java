package com.dataart.service.Impl;

import com.dataart.dao.UserRepository;
import com.dataart.model.User;
import com.dataart.security.UserRoles;
import com.dataart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        final User user = userRepository.findByEmail(s);
        if (user == null) {
            throw new UsernameNotFoundException("No user for username " + s);
        }
        return user;
    }

    @Override
    public User saveNewUser(User newUser) {
        if (userRepository.isEmailExist(newUser.getEmail())) {
            return null;
        }
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        newUser.setPassword(encoder.encode(newUser.getPassword()));
        if (newUser.getDeveloper()) {
            newUser.setRole(UserRoles.ROLE_DEVELOPER);
        } else {
            newUser.setRole(UserRoles.ROLE_USER);
        }
        return userRepository.save(newUser);
    }
}
