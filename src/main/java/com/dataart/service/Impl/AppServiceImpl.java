package com.dataart.service.Impl;

import com.dataart.dao.AppRepository;
import com.dataart.model.App;
import com.dataart.model.BigPicture;
import com.dataart.model.Category;
import com.dataart.model.SmallPicture;
import com.dataart.service.AppService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@Service
public class AppServiceImpl implements AppService {

    private static final String NAME_STRING = "name:";
    private static final String PACKAGE_STRING = "package:";
    private static final String PICTURE_128_STRING = "picture_128:";
    private static final String PICTURE_512_STRING = "picture_512:";

    private final Logger log = LoggerFactory.getLogger(AppServiceImpl.class);

    private final AppRepository appRepository;

    @Autowired
    public AppServiceImpl(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    @Override
    public Page<App> findAppsByCategory(Category category, Integer page) {
        return appRepository.findByCategory(category, new PageRequest(page == null ? 0 : page - 1, 4));
    }

    @Override
    public List<App> findPopApps() {
        return appRepository.findPopApps();
    }

    @Override
    public App findOne(Integer id) {
        return appRepository.findOne(id);
    }

    @Override
    public String save(App app, MultipartFile file) {
        if (file.getContentType().equals("application/zip") ||
                file.getContentType().endsWith("application/x-zip-compressed")) {
            try {
                ZipInputStream zipStream = new ZipInputStream(file.getInputStream());
                Map<String, byte[]> picsByName = new HashMap<>();
                String smallPicName = "";
                String bigPicName = "";
                for (ZipEntry entry; (entry = zipStream.getNextEntry()) != null; ) {
                    String entryName = entry.getName();
                    if (entryName.endsWith(".txt")) { // parse txt file
                        Scanner sc = new Scanner(zipStream);
                        while (sc.hasNextLine()) {
                            String nextLine = sc.nextLine();
                            if (nextLine.startsWith(NAME_STRING)) {
                                String newName = nextLine.replace(NAME_STRING, "").trim();
                                if (!app.getName().equals(newName)) {
                                    app.setName(newName + "/" + app.getName());
                                }
                            }
                            if (nextLine.startsWith(PACKAGE_STRING)) {
                                app.setPack(nextLine.replace(PACKAGE_STRING, "").trim());
                            }
                            if (nextLine.startsWith(PICTURE_128_STRING)) {
                                smallPicName = nextLine.replace(PICTURE_128_STRING, "").trim();
                            }
                            if (nextLine.startsWith(PICTURE_512_STRING)) {
                                bigPicName = nextLine.replace(PICTURE_512_STRING, "").trim();
                            }
                        }
                    } else { // if file is not txt, trying to parse it like an image
                        picsByName.put(entryName.substring(
                                entryName.lastIndexOf("/") + 1, entryName.length()),
                                getImageFromInputStream(zipStream));
                    }
                }

                for (String entryName : picsByName.keySet()) {
                    if (entryName.equals(smallPicName) && !smallPicName.equals("")) {
                        app.setSmallPicture(new SmallPicture(smallPicName,
                                picsByName.get(smallPicName)));
                    }
                    if (entryName.equals(bigPicName) && !bigPicName.equals("")) {
                        app.setBigPicture(new BigPicture(bigPicName,
                                picsByName.get(bigPicName)));
                    }
                }
                zipStream.close();
                return appRepository.save(app).getId().toString();
            } catch (IOException e) {
                log.error("Cannot parse zip file", e);
            }
        }
        return null;
    }

    private byte[] getImageFromInputStream(InputStream in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            for (int n; (n = in.read(buffer)) != -1; )
                out.write(buffer, 0, n);
            out.close();
            return out.toByteArray();
        } catch (IOException e) {
            log.error("Cannot get image from input stream", e);
        }
        return null;
    }

    @Override
    public byte[] downloadApp(Integer id) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
            ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
            App app = appRepository.findOne(id);
            List<File> filesInArchive = new ArrayList<>();
            // creating config for txt file
            List<String> txtLines = new ArrayList<>();
            txtLines.add(NAME_STRING + app.getName());
            txtLines.add(PACKAGE_STRING + app.getPack());
            // creating 128x128 picture, if app has it
            if (app.getSmallPicture() != null) {
                String smallPicName = app.getSmallPicture().getName();
                txtLines.add(PICTURE_128_STRING + smallPicName);
                BufferedImage bi = ImageIO.read(new ByteArrayInputStream(app.getSmallPicture().getImage()));
                File smallPic = new File(smallPicName);
                ImageIO.write(bi,
                        smallPicName.substring(
                                smallPicName.lastIndexOf(".") + 1, smallPicName.length()),
                        smallPic);
                filesInArchive.add(smallPic);
            }
            // doing the same for picture 512x512
            if (app.getBigPicture() != null) {
                String bigPicName = app.getBigPicture().getName();
                txtLines.add(PICTURE_512_STRING + bigPicName);
                BufferedImage bi = ImageIO.read(new ByteArrayInputStream(app.getBigPicture().getImage()));
                File bigPic = new File(bigPicName);
                ImageIO.write(bi,
                        bigPicName.substring(
                                bigPicName.lastIndexOf(".") + 1, bigPicName.length()),
                        bigPic);
                filesInArchive.add(bigPic);
            }
            Path txtFile = Paths.get("app.txt");
            Files.write(txtFile, txtLines, Charset.forName("UTF-8"));
            filesInArchive.add(txtFile.toFile());

            // putting files in zip archive
            for (File file : filesInArchive) {
                zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
                FileInputStream fileInputStream = new FileInputStream(file);
                IOUtils.copy(fileInputStream, zipOutputStream);
                fileInputStream.close();
                zipOutputStream.closeEntry();
            }

            zipOutputStream.close();
            bufferedOutputStream.close();
            byteArrayOutputStream.close();
            // incrementing app downloads
            app.setDownloads(app.getDownloads() + 1);
            appRepository.save(app);

            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            log.error("Cannot produce zip file", e);
        }
        return new byte[0];
    }
}
