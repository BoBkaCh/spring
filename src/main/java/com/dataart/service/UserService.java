package com.dataart.service;

import com.dataart.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User saveNewUser(User newUser);
}
