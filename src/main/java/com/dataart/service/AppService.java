package com.dataart.service;


import com.dataart.model.App;
import com.dataart.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AppService {

    Page<App> findAppsByCategory(Category category, Integer page);

    List<App> findPopApps();

    App findOne(Integer id);

    String save(App app, MultipartFile file);

    byte[] downloadApp(Integer id);
}
