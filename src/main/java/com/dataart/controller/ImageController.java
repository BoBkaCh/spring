package com.dataart.controller;

import com.dataart.service.ImageService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("/image")
public class ImageController {

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public void showImage(@PathVariable Integer id,
                            HttpServletResponse response) throws IOException {
        InputStream is = new ByteArrayInputStream(imageService.find(id));
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(is, response.getOutputStream());
    }
}
