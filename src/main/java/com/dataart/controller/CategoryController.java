package com.dataart.controller;

import com.dataart.model.App;
import com.dataart.model.Category;
import com.dataart.service.AppService;
import com.dataart.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CategoryController {

    private final CategoryService categoryService;

    private final AppService appService;


    @Autowired
    public CategoryController(CategoryService categoryService, AppService appService) {
        this.categoryService = categoryService;
        this.appService = appService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showHomePage(){
        return "redirect:/category/" + Category.FIRST_CATEGORY_ID;
    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
    public String showCategory(@PathVariable Integer id, Model model,
                               HttpServletRequest request,
                               @RequestParam(required = false) Integer page){
        boolean pjax = Boolean.TRUE.toString().equals(request.getHeader("X-PJAX"));
        Category currentCategory = null;
        Iterable<Category> categories = categoryService.findAll();
        for (Category category : categories){
            if (category.getId().equals(id)){
                currentCategory = category;
                break;
            }
        }
        Page<App> categoryAppsPage = appService.findAppsByCategory(currentCategory, page);
        model.addAttribute("category", currentCategory);
        model.addAttribute("categories", categories);
        model.addAttribute("content", "category-fragment");
        model.addAttribute("apps", categoryAppsPage.getContent());
        model.addAttribute("pages", new Integer[categoryAppsPage.getTotalPages()]);
        model.addAttribute("popApps", appService.findPopApps());
        return pjax ? "category-fragment" : "home";
    }
}
