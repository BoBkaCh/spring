package com.dataart.controller;

import com.dataart.model.App;
import com.dataart.model.Category;
import com.dataart.service.AppService;
import com.dataart.service.CategoryService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Controller
@RequestMapping("/app")
public class AppController {

    private final AppService appService;

    private final CategoryService categoryService;

    @Autowired
    public AppController(AppService appService, CategoryService categoryService) {
        this.appService = appService;
        this.categoryService = categoryService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showApp(@PathVariable Integer id,
                          Model model, HttpServletRequest request) {
        boolean pjax = Boolean.TRUE.toString().equals(request.getHeader("X-PJAX"));
        model.addAttribute("app", appService.findOne(id));
        if (!pjax) {
            model.addAttribute("content", "app-fragment");
            model.addAttribute("categories", categoryService.findAll());
            model.addAttribute("popApps", appService.findPopApps());
        }
        return pjax ? "app-fragment" : "home";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    @Secured("ROLE_DEVELOPER")
    public String uploadApp(@RequestParam("file") MultipartFile file,
                            @RequestParam("appName") String appName,
                            @RequestParam("appDesc") String appDesc,
                            @RequestParam("category") Integer categoryId) throws IOException {
        App newApp = new App();
        newApp.setName(appName);
        newApp.setDescription(appDesc);
        newApp.setCategory(new Category(categoryId));
        newApp.setDownloads(0);
        return appService.save(newApp, file);
    }

    @RequestMapping(value = "/download/{id}", method = RequestMethod.GET, produces="application/zip")
    public void downloadApp(@PathVariable Integer id,
                              HttpServletResponse response) throws IOException {
        response.setContentType("application/zip");
        IOUtils.copy(new ByteArrayInputStream(appService.downloadApp(id)), response.getOutputStream());
    }
}
