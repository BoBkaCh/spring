package com.dataart.dao;

import com.dataart.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String email);

    @Query("SELECT CASE WHEN COUNT(u) > 0 " +
            "THEN 'true' ELSE 'false' END " +
            "FROM users u WHERE u.email = ?1")
    Boolean isEmailExist(String email);
}
