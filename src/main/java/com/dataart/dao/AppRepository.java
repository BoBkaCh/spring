package com.dataart.dao;

import com.dataart.model.App;
import com.dataart.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppRepository extends PagingAndSortingRepository<App, Integer> {

    Page<App> findByCategory(Category category, Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT * FROM app a ORDER BY a.downloads DESC LIMIT 10")
    List<App> findPopApps();
}
