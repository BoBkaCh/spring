package com.dataart.dao;

import com.dataart.model.SmallPicture;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends CrudRepository<SmallPicture, Integer>{
}
