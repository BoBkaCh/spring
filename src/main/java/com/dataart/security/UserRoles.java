package com.dataart.security;

import org.springframework.security.core.GrantedAuthority;

public enum UserRoles implements GrantedAuthority {
    ROLE_USER,
    ROLE_DEVELOPER;

    @Override
    public String getAuthority() {
        return toString();
    }
}
