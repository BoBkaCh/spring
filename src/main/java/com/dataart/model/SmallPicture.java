package com.dataart.model;

import javax.persistence.*;

@Entity(name = "picture")
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="size",
        discriminatorType= DiscriminatorType.STRING      )
@DiscriminatorValue("small")
public class SmallPicture implements DomainObject{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer id;

    @Column
    private String name;

    @Column
    private byte[] image;

    public SmallPicture() {
    }

    public SmallPicture(String name, byte[] image) {
        this.name = name;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
