package com.dataart.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("big")
public class BigPicture extends SmallPicture {

    public BigPicture() {
    }

    public BigPicture(String name, byte[] image) {
        super(name, image);
    }
}
