package com.dataart.model;

import javax.persistence.*;

@Entity(name = "app")
public class App implements DomainObject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer id;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn
    private Category category;

    @OneToOne
    @JoinColumn
    private User developer;

    @Column
    private String description;

    @Column
    private Integer downloads;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn
    private SmallPicture smallPicture;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn
    private BigPicture bigPicture;

    @Column
    private String pack;

    public App() {
    }

    public App(String name, Category category, User developer, String description, Integer downloads, SmallPicture smallPicture, BigPicture bigPicture, String pack) {
        this.name = name;
        this.category = category;
        this.developer = developer;
        this.description = description;
        this.downloads = downloads;
        this.smallPicture = smallPicture;
        this.bigPicture = bigPicture;
        this.pack = pack;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getDeveloper() {
        return developer;
    }

    public void setDeveloper(User developer) {
        this.developer = developer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDownloads() {
        return downloads;
    }

    public void setDownloads(Integer downloads) {
        this.downloads = downloads;
    }

    public SmallPicture getSmallPicture() {
        return smallPicture;
    }

    public void setSmallPicture(SmallPicture smallPicture) {
        this.smallPicture = smallPicture;
    }

    public BigPicture getBigPicture() {
        return bigPicture;
    }

    public void setBigPicture(BigPicture bigPicture) {
        this.bigPicture = bigPicture;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }
}
